/* global module */
/* eslint comma-dangle: ["error", "always-multiline"] */
module.exports = {
    "extends": "eslint:recommended",
    "rules": {
        // enable additional rules
        "indent": ["error", 4],
        "linebreak-style": ["error", "unix"],
        "quotes": ["error", "double"],
        "semi": ["error", "always"],
        "max-len": ["error", 88],
        "no-trailing-spaces": ["error"],
        "no-multiple-empty-lines": ["error", {"max": 1, "maxEOF": 0}],

        // override default options for rules from base configurations
        "comma-dangle": ["error", "never"],
        "no-cond-assign": ["error", "always"],

        // disable rules from base configurations
        "no-console": "off",
    },
};
